﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CMS_Master.Startup))]
namespace CMS_Master
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
